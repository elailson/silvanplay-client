import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CadastraInstituicaoEducacionalComponent } from './pages/cadastra-instituicao-educacional/cadastra-instituicao-educacional.component';
import { LoginComponent } from './pages/login/login.component';
import { CadastraTelefoneComponent } from './pages/cadastra-telefone/cadastra-telefone.component';
import { GerenciaSolicitacaoComponent } from './pages/gerencia-solicitacao/gerencia-solicitacao.component';
import { AuthGuardPerfil2 } from './auth/guards/auth.guardPerfil2';
import { HomeComponent } from './pages/home/home.component';
import { CadastraAdministradorComponent } from './pages/cadastra-administrador/cadastra-administrador.component';
import { AuthGuardPerfil7 } from './auth/guards/auth.guardPerfil7';

const routes: Routes = [
  { path: '', component: LoginComponent },
  { path: 'login', component: LoginComponent },
  { path: 'home', component: HomeComponent },
  { path: 'cadastra-instituicao', component: CadastraInstituicaoEducacionalComponent, canActivate: [AuthGuardPerfil2] },
  { path: 'gerencia-solicitacoes', component: GerenciaSolicitacaoComponent },
  { path: 'cadastra-administrador', component: CadastraAdministradorComponent, canActivate: [AuthGuardPerfil7] },
  { path: 'cadastra-telefone', component: CadastraTelefoneComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
