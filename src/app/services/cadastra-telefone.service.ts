import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from '../../environments/environment';
import { environmentProd } from '../../environments/environment.prod';
import { Instituicao } from '../model/Instituicao';
import { Telefone } from '../model/Telefone';
import { LocalStorageService } from 'angular-web-storage';

@Injectable({providedIn: 'root'})
export class CadastroTelefoneService {

    private relativePath: string = environment.url;
    // private relativePath: string = environmentProd.url;

    private token = this.storageService.get('token');

    constructor(private http: HttpClient, private storageService: LocalStorageService) { }

    cadastrar(telefone: Telefone) {
        return this.http.post(this.relativePath + 'telefone', telefone, { headers: this.getHeaders() });
    }

    protected getHeaders() {
        console.log('TOKEN SERVICE TELEFONE => ' + this.token);
        const headers = new HttpHeaders();
        headers.set('Content-Type', 'application/json');
        headers.set('Authorization', this.token);
        return headers;
    }
}