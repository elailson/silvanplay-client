import { Component, OnInit, ViewChild } from '@angular/core';
import { GerenciaSolicitacaoService } from 'src/app/services/gerencia-solicitacao.service';
import { MatTableDataSource } from '@angular/material/table';
import { Solicitacao } from 'src/app/model/Solicitacao';
import { ToasterService } from 'angular2-toaster';
import { BlockUI, NgBlockUI } from 'ng-block-ui';
import { MatSort } from '@angular/material/sort';

@Component({
  selector: 'app-gerencia-solicitacao',
  templateUrl: './gerencia-solicitacao.component.html',
  styleUrls: ['./gerencia-solicitacao.component.css']
})
export class GerenciaSolicitacaoComponent implements OnInit {

  @ViewChild(MatSort, {static: false}) sort: MatSort;
  @BlockUI() blockUI: NgBlockUI;
  dataSource: MatTableDataSource<Solicitacao>;

  displayedColumns: string[] = ['idSolicitacao', 'bairro', 'rua', 'categoria', 'descricao', 'acoes'];

  constructor(private gerenciaSolicitacaoService: GerenciaSolicitacaoService,
    private toasterService: ToasterService) { }

  ngOnInit() {
    this.blockUI.start('Buscando solicitações');
    this.gerenciaSolicitacaoService.buscaSolicitacoes()
      .subscribe(solicitacoes => {
        this.dataSource = new MatTableDataSource<Solicitacao>(solicitacoes);
        this.dataSource.sort = this.sort;
        this.blockUI.stop();
      }, error => {
        this.toasterService.pop('warning', 'Erro ao buscar solicitações')
        this.blockUI.stop();
      });
  }

  getCategoria(categoria: number): string {
    switch (categoria) {
      case 0:
        return 'Iluminação pública';
      case 1:
        return 'Infração de trânsito';
      case 2:
        return 'Má conservação de patrimônio';
      case 3:
        return 'Outros';
    }
  }

}
